import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router } from 'react-router-dom';
import './common.css';
import { App } from './Components/App/App';
import { initAPI } from './api';
import { AuthContextProvider } from './features/Auth/AuthContext';

const firebaseApp = initAPI();

ReactDOM.render(
  <AuthContextProvider firebaseApp={firebaseApp}>
    <Router>
      <App />
    </Router>
  </AuthContextProvider>,
  document.getElementById('root')
);
