import React, { createContext, useContext, useEffect, useState } from 'react';
import {
  browserSessionPersistence,
  getAuth,
  GithubAuthProvider,
  GoogleAuthProvider,
  ProviderId,
  signInWithEmailAndPassword,
  signInWithPopup,
  signOut,
  UserCredential,
} from 'firebase/auth';
import { TAuthContext } from './types';
import { FirebaseApp } from 'firebase/app';
import { isUserAdmin } from '../../api';

type AuthContextProviderProps = {
  children: React.ReactNode;
  firebaseApp: FirebaseApp;
};

export const ALLOWED_OAUTH_PROVIDERS: Record<string, any> = {
  [ProviderId.GOOGLE]: new GoogleAuthProvider(),
  [ProviderId.GITHUB]: new GithubAuthProvider(),
};

export const authContext = createContext<TAuthContext>({
  isAuthenticated: null,
  loginWithEmailAndPassword: () => Promise.reject({}),
  loginWithPopup: () => Promise.reject({}),
  logOut: () => void 0,
});

export const useAuthContext = (): TAuthContext => {
  return useContext<TAuthContext>(authContext);
};

export const AuthContextProvider = ({ children, firebaseApp }: AuthContextProviderProps) => {
  const [isAuthenticated, setIsAuthenticated] = useState<TAuthContext['isAuthenticated']>(null);
  const [user, setUser] = useState<any>(null);
  const [auth] = useState(getAuth(firebaseApp));

  useEffect(() => {
    if (!auth) {
      return;
    }
    auth.setPersistence(browserSessionPersistence);
    auth.languageCode = 'ru';

    auth.onAuthStateChanged((user) => {
      // console.log('auth changed', user);
      if (user) {
        isUserAdmin()
          .then(() => {
            setUser(user);
            setIsAuthenticated(true);
          })
          .catch(() => {
            logOut();
            setUser(null);
            setIsAuthenticated(false);
          });
      } else {
        setUser(null);
        setIsAuthenticated(false);
      }
    });
  }, [auth]);

  const processLogin = async (loginPromise: Promise<UserCredential>): Promise<UserCredential> => {
    setUser(null);
    setIsAuthenticated(null);

    return loginPromise
      .then((result) => {
        // log success auth
        return result;
      })
      .catch((error) => {
        // log auth errors
        throw error;
      });
  };

  const loginWithEmailAndPassword = (email: string, password: string) => {
    return processLogin(signInWithEmailAndPassword(auth, email, password));
  };

  const loginWithPopup = (provider: string) => {
    return processLogin(signInWithPopup(auth, ALLOWED_OAUTH_PROVIDERS[provider]));
  };

  const logOut = () => signOut(auth);

  return (
    <authContext.Provider
      value={{
        isAuthenticated,
        user,
        loginWithEmailAndPassword,
        loginWithPopup,
        logOut,
      }}
    >
      {children}
    </authContext.Provider>
  );
};
