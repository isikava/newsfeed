import { UserCredential } from 'firebase/auth';

export type TAuthContext = {
  // boolean - определенное состояние. null - неизвестное (загрузка)
  isAuthenticated: boolean | null;
  user?: any;
  loginWithEmailAndPassword: (email: string, password: string) => Promise<UserCredential>;
  loginWithPopup: (provider: string) => Promise<UserCredential>;
  logOut: () => void;
};
