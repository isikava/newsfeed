import React from 'react';
import './SingleLineTitleArticle.css';
import { Link } from 'react-router-dom';

type SingleLineTitleArticleProps = {
  id: number;
  image: string;
  title: string;
  text: string;
  category: string;
  source: string;
};

export const SingleLineTitleArticle = ({ id, image, title, text, category, source }: SingleLineTitleArticleProps) => {
  return (
    <Link to={`/article/${id}`} className='single-line-title-article'>
      <article>
        <div className='article-img single-line-title-article__image-container'>
          <img className='single-line-title-article__image' src={image} />
        </div>
        <span className='article-category single-line-title-article__category'>{category}</span>
        <h2 className='single-line-title-article__title'>{title}</h2>
        <p className='single-line-title-article__text'>{text}</p>
        <span className='article-source single-line-title-article__source'>{source}</span>
      </article>
    </Link>
  );
};
