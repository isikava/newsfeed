import React from 'react';
import './Page.css';
import { Navigation } from '../Navigation/Navigation';
import { Logo } from '@components/Logo/Logo';

type PageProps = {
  children: React.ReactNode;
};

export const Page = ({ children }: PageProps) => {
  return (
    <>
      <header className='header'>
        <div className='container header__container'>
          <Logo />
          <Navigation className='header__navigation' />
        </div>
      </header>

      <main>{children}</main>

      <footer className='footer'>
        <div className='container'>
          <div className='footer__top'>
            <Logo />
            <Navigation className='footer__navigation' />
          </div>

          <div className='footer__bottom'>Isikava</div>
        </div>
      </footer>
    </>
  );
};
