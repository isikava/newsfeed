import { Redirect, Route, RouteProps } from 'react-router-dom';
import React from 'react';
import { Box, CircularProgress } from '@mui/material';
import { useAuthContext } from '../../features/Auth/AuthContext';

type PrivateRouteProps = {
  children: React.ReactNode;
} & RouteProps;

export const PrivateRoute = ({ children, ...rest }: PrivateRouteProps) => {
  const { isAuthenticated } = useAuthContext();

  if (isAuthenticated === null) {
    return (
      <Box sx={{ p: 4, textAlign: 'center' }}>
        <CircularProgress color='primary' />
      </Box>
    );
  }
  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthenticated ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};
