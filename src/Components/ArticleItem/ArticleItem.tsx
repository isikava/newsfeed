import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import './ArticleItem.css';
import { RelatedSmallArticle } from '../RelatedSmallArticle/RelatedSmallArticle';
import { SingleLineTitleArticle } from '../SingleLineTitleArticle/SingleLineTitleArticle';
import { Article, ArticleItemAPI, Category, RelatedArticlesAPI, Source } from '../../types';
import { ArticleItemInfo } from '../ArticleItemInfo/ArticleItemInfo';

export const ArticleItem = () => {
  const { id }: { id: string } = useParams();
  const [articleItem, setArticleItem] = useState<ArticleItemAPI | null>(null);
  const [relatedArticles, setRelatedArticles] = useState<Article[] | null>(null);
  const [categories, setCategories] = useState<Category[]>([]);
  const [sources, setSources] = useState<Source[]>([]);

  React.useEffect(() => {
    fetch(`https://frontend.karpovcourses.net/api/v2/news/full/${id}`)
      .then((response) => response.json())
      .then((response: ArticleItemAPI) => {
        setArticleItem(response);
      });

    Promise.all([
      fetch(`https://frontend.karpovcourses.net/api/v2/news/related/${id}?count=9`).then((res) => res.json()),
      fetch(`https://frontend.karpovcourses.net/api/v2/categories`).then((res) => res.json()),
      fetch(`https://frontend.karpovcourses.net/api/v2/sources`).then((res) => res.json()),
    ]).then((responses) => {
      const articles: RelatedArticlesAPI = responses[0];
      const categories: Category[] = responses[1];
      const sources: Source[] = responses[2];

      setRelatedArticles(articles.items);
      setCategories(categories);
      setSources(sources);
    });
  }, [id]);

  if (articleItem === null || relatedArticles === null) {
    return null;
  }

  return (
    <section className='article-page'>
      <article className='article'>
        {articleItem.image.length > 0 ? (
          <section
            className='article__hero'
            style={{
              backgroundImage: `url(${articleItem.image})`,
            }}
          >
            <div className='container article__hero-content'>
              <div className='grid'>
                <h1 className='article__hero-title'>{articleItem.title}</h1>
              </div>

              <ArticleItemInfo
                categoryName={articleItem.category.name}
                date={articleItem.date}
                sourceLink={articleItem.link}
                sourceName={articleItem.source.name}
                author={articleItem.author}
              />
            </div>
          </section>
        ) : null}

        <div className='grid container article__main'>
          <div className='article__content'>
            {!articleItem.image.length && (
              <div className='article__title-container'>
                <h1 className='article__title'>{articleItem.title}</h1>

                <ArticleItemInfo
                  categoryName={articleItem.category.name}
                  date={articleItem.date}
                  sourceLink={articleItem.link}
                  sourceName={articleItem.source.name}
                  author={articleItem.author}
                />
              </div>
            )}

            <p>{articleItem.text}</p>
          </div>

          <div className='article__small-column'>
            {relatedArticles.slice(3, 9).map((item) => {
              const category = categories.find(({ id }) => item.category_id === id);
              const source = sources.find(({ id }) => item.source_id === id);
              return (
                <RelatedSmallArticle
                  key={item.id}
                  id={item.id}
                  title={item.title}
                  category={category?.name || ''}
                  source={source?.name || ''}
                  image={item.image}
                />
              );
            })}
          </div>
        </div>
      </article>

      <section className='article-page__related-articles'>
        <div className='container'>
          <h2 className='article-page__related-articles-title'>Читайте также:</h2>

          <div className='grid article-page__related-articles-list'>
            {relatedArticles.slice(0, 3).map((item) => {
              const category = categories.find(({ id }) => item.category_id === id);
              const source = sources.find(({ id }) => item.source_id === id);
              return (
                <SingleLineTitleArticle
                  key={item.id}
                  id={item.id}
                  title={item.title}
                  text={item.description}
                  category={category?.name || ''}
                  source={source?.name || ''}
                  image={item.image}
                />
              );
            })}
          </div>
        </div>
      </section>
    </section>
  );
};
