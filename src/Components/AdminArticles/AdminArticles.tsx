import * as React from 'react';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Grid from '@mui/material/Grid';
import { Button, CardActionArea } from '@mui/material';
import { IPartnerArticle } from '../../types';
import { getPartnersArticles } from '../../api';

export const AdminArticles = () => {
  const [articles, setArticles] = useState<IPartnerArticle[]>([]);

  useEffect(() => {
    const getData = async () => {
      const data = await getPartnersArticles();
      setArticles(data);
    };

    getData();
  }, []);
  return (
    <>
      <Grid container spacing={2} sx={{ mb: 3 }}>
        <Grid item xs={9}>
          <Typography variant='h4' gutterBottom>
            Список статей
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Button variant='contained' color='success' sx={{ mr: 1 }} component={Link} to='admin/create'>
              Добавить
            </Button>
          </Box>
        </Grid>
      </Grid>

      <Grid container spacing={2}>
        {articles.map((item) => (
          <Grid item xs={3} key={item.id}>
            <Card>
              <CardActionArea component={Link} to={`/admin/edit/${item.id}`}>
                <CardMedia component='img' height='140' image={item.image} alt={item.title} />
                <CardContent>
                  <Typography gutterBottom variant='h5' component='div'>
                    {item.title}
                  </Typography>
                  <Typography variant='body2' color='text.secondary'>
                    {item.description}
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        ))}
      </Grid>
    </>
  );
};
