import React from 'react';
import { Link } from 'react-router-dom';
import './SmallArticle.css';
import { formatDate } from '../../utils';

type SmallArticleProps = {
  id: number;
  title: string;
  source: string;
  date: string;
};

export const SmallArticle = ({ id, title, source, date }: SmallArticleProps) => {
  return (
    <Link to={`/article/${id}`} className='small-article'>
      <article className='small-article__container'>
        <h2 className='small-article__title'>{title}</h2>
        <span className='article-date'>{source}</span>
        <span className='article-source'>{formatDate(date)}</span>
      </article>
    </Link>
  );
};
