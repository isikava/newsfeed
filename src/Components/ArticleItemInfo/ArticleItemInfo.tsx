import React from 'react';
import './ArticleItemInfo.css';
import { formatDate } from '../../utils';

type ArticleItemInfoProps = {
  categoryName: string;
  date: string;
  sourceLink?: string;
  sourceName?: string;
  author?: string;
};

export const ArticleItemInfo = ({ categoryName, date, sourceName, sourceLink, author }: ArticleItemInfoProps) => {
  return (
    <div className='grid'>
      <div className='article-item-info__category-container'>
        <span className='article-category article-item-info__category'>{categoryName}</span>
        {sourceLink && (
          <a href={sourceLink} target='_blank' rel='noreferrer' className='article-item-info__link'>
            Источник: {sourceName}
            {author && <span className='article-item-info__author'>({author})</span>}
          </a>
        )}
      </div>

      <span className='article-date article-item-info__date'>{formatDate(date)}</span>
    </div>
  );
};
